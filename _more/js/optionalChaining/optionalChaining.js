const articles = [
  {
    id: 101,
    title: 'First Article',
    author: { id: 101, name: 'John' },
  },
  {
    id: 102,
    title: 'Second Article',
  },
  {
    id: 103,
    title: 'Article 3',
    author: { id: 102, name: 'Austin' },
  },
]
articles.forEach(article => {
  // const authorName = article.author.name;
  // const authorName = article?[author]?['name'];
  const authorName = article?.author?.name;
  console.log('authorName=', authorName)
});