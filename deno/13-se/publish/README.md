# Publish

* [How to Add Third Party Library Deno.js ?](https://www.geeksforgeeks.org/how-to-add-third-party-library-deno-js/)

```
Now comes the Publishing part-

Add your module to your Github and enable the actions from your repo.
Make the repo public.
Now go to DATABASE.JSON.
Add your module details in the following format.
"my_library_name": {
    "type": "github",
    "owner": "",
    "repo": ""
}
Now create pull request to update the file.
Write “Add `your_module_name` to Deno.
Make Pull Request.
Now your updated file need to pass some tests and you are good to go.
Successfully created pull request. Wait for the approval.
```
