class format {  
    public static void main(String args[]) throws Exception 
    {
        System.out.println(String.format("|%08d|", 3));
        System.out.println(String.format("|%15s|", "hi"));
        String hex = "2cf24dba5fb0a30e26e83b2ac5b9e29e1b161e5c1fa7425e73043362938b9824";
        System.out.println(String.format("|%64s|", hex));
    }
}
