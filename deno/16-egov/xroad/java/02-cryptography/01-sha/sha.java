// https://www.geeksforgeeks.org/sha-256-hash-in-java/
import java.math.BigInteger;  
import java.nio.charset.StandardCharsets; 
import java.security.MessageDigest;

class SHA {  
    public static String sha256(String input) throws Exception 
    {  
        MessageDigest md = MessageDigest.getInstance("SHA-256");  
        byte[] hash = md.digest(input.getBytes(StandardCharsets.UTF_8));
        BigInteger number = new BigInteger(1, hash);
        String hex = number.toString(16);
        return hex;
    }
    
    public static void main(String args[]) throws Exception 
    {
        System.out.println("sha256(hello)       ="+sha256("hello"));
        System.out.println("sha256(hello!)      ="+sha256("hello!"));
        System.out.println("sha256(hello world!)="+sha256("hello world!"));
    }  
}