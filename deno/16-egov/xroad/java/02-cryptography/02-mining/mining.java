// https://www.geeksforgeeks.org/sha-256-hash-in-java/
import java.math.BigInteger;  
import java.nio.charset.StandardCharsets; 
import java.security.MessageDigest;

class Block {
    int nonce;
    String data;
    
    Block(int pNonce, String pData) {
      nonce = pNonce;
      data = pData;
    }

    public String toString() {
      return String.format("<block nonce='%d'><data>%s</data>", nonce, data);
    }
}

class mining {  
    public static String sha256(String input) throws Exception 
    {  
        MessageDigest md = MessageDigest.getInstance("SHA-256");  
        byte[] hash = md.digest(input.getBytes(StandardCharsets.UTF_8));
        BigInteger number = new BigInteger(1, hash);
        String hex = number.toString(16);
        return String.format("%64s", hex).replace(' ', '0');
    }
    
    public static Block mining(Block block) throws Exception
    {
      for (int i=0; i<100000000; i++) {
        block.nonce = i;
        String h = sha256(block.toString());
        if (h.startsWith("0000")) return block;
      }
      return null;
    }

    public static void main(String args[]) throws Exception 
    {
        System.out.println("sha256(hello)       ="+sha256("hello")+"|");
        System.out.println("sha256(hello!)      ="+sha256("hello!"));
        System.out.println("sha256(hello world!)="+sha256("hello world!"));

        Block block = new Block(0, "john => mary : $2.7; george => john : $1.3");
        Block mb = mining(block);
        System.out.println("mining="+mb.toString());
    }  
}
