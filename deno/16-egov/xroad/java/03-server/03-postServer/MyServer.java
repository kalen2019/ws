import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URI;
import java.nio.file.Files;
import java.net.InetSocketAddress;

import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;

class MyHandler implements HttpHandler {

  private final String baseDir;

  public MyHandler(String baseDir) {
    this.baseDir = baseDir;
  }

  @Override
  public void handle(HttpExchange ex) throws IOException {
    URI uri = ex.getRequestURI();
    String path = uri.getPath();
    File file = new File(baseDir, path);
    System.out.println("uri="+uri);
    System.out.println("path="+path);
    System.out.println("absolutePath="+file.getAbsolutePath());
    Headers h = ex.getResponseHeaders();
    if (path.endsWith(".html")) {
      h.add("Content-Type", "text/html");
    } else {
      h.add("Content-Type", "text/plain; charset=utf-8");
    }
    OutputStream out = ex.getResponseBody();
    if (file.exists()) {
      ex.sendResponseHeaders(200, file.length());
      out.write(Files.readAllBytes(file.toPath()));
    } else {
      System.err.println("File not found: " + file.getAbsolutePath());
      ex.sendResponseHeaders(404, 0);
      out.write("404 File not found.".getBytes());
    }
    out.close();
  }
}

public class MyServer {
    public static void main(String[] args) throws Exception {
        HttpServer server = HttpServer.create(new InetSocketAddress(8080), 0);
        System.out.println("server run at http://localhost:8080/");
        server.createContext("/", new MyHandler("./"));
        server.setExecutor(null); // creates a default executor
        server.start();
    }
}