# AKKA

* https://akka.io/

Akka is a toolkit for building highly concurrent, distributed, and resilient message-driven applications for Java and Scala

* https://developer.lightbend.com/guides/akka-quickstart-java/

## akka-quickstart-java

run:

```
user@DESKTOP-96FRN6B MINGW64 /d/ccc109/egov/java/akka/akka-quickstart-java (master)
$ gradle run

> Task :run
[2021-01-03 09:19:14,998] [INFO] [akka.event.slf4j.Slf4jLogger] [helloakka-akka.actor.default-dispatcher-6] [] - Slf4jLogger started
>>> Press ENTER to exit <<<
[2021-01-03 09:19:15,535] [INFO] [com.example.Greeter] [helloakka-akka.actor.default-dispatcher-3] [akka://helloakka/user/greeter] - Hello Charles!
[2021-01-03 09:19:15,547] [INFO] [com.example.GreeterBot] [helloakka-akka.actor.default-dispatcher-3] [akka://helloakka/user/Charles] - Greeting 1 for Charles
[2021-01-03 09:19:15,549] [INFO] [com.example.Greeter] [helloakka-akka.actor.default-dispatcher-3] [akka://helloakka/user/greeter] - Hello Charles!
[2021-01-03 09:19:15,550] [INFO] [com.example.GreeterBot] [helloakka-akka.actor.default-dispatcher-3] [akka://helloakka/user/Charles] - Greeting 2 for Charles
[2021-01-03 09:19:15,551] [INFO] [com.example.Greeter] [helloakka-akka.actor.default-dispatcher-6] [akka://helloakka/user/greeter] - Hello Charles!
[2021-01-03 09:19:15,552] [INFO] [com.example.GreeterBot] [helloakka-akka.actor.default-dispatcher-6] [akka://helloakka/user/Charles] - Greeting 3 for Charles
<=========----> 75% EXECUTING [4m 36s]
> IDLE
> IDLE
> :run
> IDLE
```

test

```
user@DESKTOP-96FRN6B MINGW64 /d/ccc109/egov/java/akka/akka-quickstart-java (master)
$ gradle test

Deprecated Gradle features were used in this build, making it incompatible with Gradle 7.0.
Use '--warning-mode all' to show the individual deprecation warnings.
See https://docs.gradle.org/6.7.1/userguide/command_line_interface.html#sec:command_line_warnings   

BUILD SUCCESSFUL in 24s
4 actionable tasks: 2 executed, 2 up-to-date
```
