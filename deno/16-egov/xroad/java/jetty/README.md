# Jetty


* https://zh.wikipedia.org/wiki/Jetty
    * Jetty是一個純粹的基於Java的網頁伺服器和Java Servlet容器。
    * https://www.eclipse.org/jetty/documentation/jetty-11/operations_guide.php
    * https://www.eclipse.org/jetty/documentation/jetty-11/programming_guide.php

* https://zh.wikipedia.org/wiki/Jetty
* https://www.eclipse.org/jetty/documentation/jetty-11/operations_guide.php
* https://www.eclipse.org/jetty/documentation/jetty-11/programming_guide.php

## 下載

* https://www.eclipse.org/jetty/download.php

## 參考

* http://index-of.es/Varios-2/Jetty-Server-Cookbook.pdf

## http

```
user@DESKTOP-96FRN6B MINGW64 /d/ccc109/egov/java/00-install/03-jetty (master)
$ java -jar $JETTY_HOME/start.jar --add-module=http
INFO  : mkdir ${jetty.base}\start.d
INFO  : server          transitively enabled, ini template available with --add-module=server
INFO  : logging-jetty   transitively enabled
INFO  : http            initialized in ${jetty.base}\start.d\http.ini
INFO  : resources       transitively enabled
INFO  : threadpool      transitively enabled, ini template available with --add-module=threadpool
INFO  : logging/slf4j   dynamic dependency of logging-jetty
INFO  : bytebufferpool  transitively enabled, ini template available with --add-module=bytebufferpool
INFO  : mkdir ${jetty.base}\resources
INFO  : copy ${jetty.home}\modules\logging\jetty\resources\jetty-logging.properties to ${jetty.base}\resources\jetty-logging.properties
INFO  : Base directory was modified
```

然後看 http://localhost:8080/ 結果如下：

![](./img/JettyHttpHello.png)

## Demo

```
user@DESKTOP-96FRN6B MINGW64 /d/ccc109/egov/java/00-install/03-jetty (master)
$ echo $JETTY_HOME
D:\install\java\jetty-home-11.0.0

user@DESKTOP-96FRN6B MINGW64 /d/ccc109/egov/java/00-install/03-jetty (master)
$ java -jar $JETTY_HOME/start.jar --add-module=demo
INFO  : mkdir ${jetty.base}\start.d
INFO  : webapp          transitively enabled, ini template available with --add-module=webapp
INFO  : websocket-jetty transitively enabled
INFO  : server          transitively enabled, ini template available with --add-module=server
INFO  : alpn            transitively enabled, ini template available with --add-module=alpn
INFO  : test-keystore   transitively enabled
INFO  : servlet         transitively enabled
INFO  : jsp             transitively enabled
INFO  : demo-realm      transitively enabled, ini template available with --add-module=demo-realm
INFO  : demo-jetty      transitively enabled
INFO  : annotations     transitively enabled
INFO  : demo-proxy      transitively enabled
INFO  : alpn-impl/alpn-11 dynamic dependency of alpn-impl/alpn-15
INFO  : jdbc            transitively enabled
INFO  : demo            initialized in ${jetty.base}\start.d\demo.ini
INFO  : threadpool      transitively enabled, ini template available with --add-module=threadpool
INFO  : ssl             transitively enabled, ini template available with --add-module=ssl
INFO  : alpn-impl/alpn-15 dynamic dependency of alpn
INFO  : deploy          transitively enabled, ini template available with --add-module=deploy
INFO  : logging-jetty   transitively enabled
INFO  : security        transitively enabled
INFO  : websocket-jakarta transitively enabled
INFO  : demo-spec       transitively enabled
INFO  : jaas            transitively enabled, ini template available with --add-module=jaas
INFO  : client          transitively enabled
INFO  : http2           transitively enabled, ini template available with --add-module=http2
INFO  : https           transitively enabled
INFO  : demo-moved-context transitively enabled
INFO  : demo-jndi       transitively enabled
INFO  : bytebufferpool  transitively enabled, ini template available with --add-module=bytebufferpool
INFO  : ext             transitively enabled
INFO  : demo-rewrite    transitively enabled
INFO  : work            transitively enabled
INFO  : resources       transitively enabled
INFO  : demo-async-rest transitively enabled
INFO  : plus            transitively enabled
INFO  : rewrite         transitively enabled, ini template available with --add-module=rewrite
INFO  : jstl            transitively enabled
INFO  : demo-mock-resources transitively enabled
INFO  : apache-jsp      transitively enabled
INFO  : jndi            transitively enabled
INFO  : servlets        transitively enabled
INFO  : http            transitively enabled, ini template available with --add-module=http
INFO  : apache-jstl     transitively enabled
INFO  : demo-jaas       transitively enabled
INFO  : logging/slf4j   dynamic dependency of logging-jetty
INFO  : demo-root       transitively enabled
INFO  : mkdir ${jetty.base}\etc
INFO  : copy ${jetty.home}\modules\test-keystore\test-keystore.p12 to ${jetty.base}\etc\test-keystore.p12
INFO  : copy ${jetty.home}\modules\demo.d\demo-realm.xml to ${jetty.base}\etc\demo-realm.xml
INFO  : copy ${jetty.home}\modules\demo.d\demo-realm.properties to ${jetty.base}\etc\demo-realm.properties
INFO  : mkdir ${jetty.base}\webapps\demo-jetty.d
INFO  : copy ${jetty.home}\modules\demo.d\demo-jetty.xml to ${jetty.base}\webapps\demo-jetty.xml
INFO  : copy ${jetty.home}\modules\demo.d\demo-jetty-override-web.xml to ${jetty.base}\webapps\demo-jetty.d\demo-jetty-override-web.xml
INFO  : download https://repo1.maven.org/maven2/org/eclipse/jetty/demos/demo-jetty-webapp/11.0.0/demo-jetty-webapp-11.0.0.war to ${jetty.base}\webapps\demo-jetty.war
INFO  : download https://repo1.maven.org/maven2/org/eclipse/jetty/demos/demo-proxy-webapp/11.0.0/demo-proxy-webapp-11.0.0.war to ${jetty.base}\webapps\demo-proxy.war
INFO  : mkdir ${jetty.base}\resources
INFO  : copy ${jetty.home}\modules\logging\jetty\resources\jetty-logging.properties to ${jetty.base}\resources\jetty-logging.properties
INFO  : copy ${jetty.home}\modules\demo.d\demo-spec.xml to ${jetty.base}\webapps\demo-spec.xml
INFO  : download https://repo1.maven.org/maven2/org/eclipse/jetty/demos/demo-spec-webapp/11.0.0/demo-spec-webapp-11.0.0.war to ${jetty.base}\webapps\demo-spec.war
INFO  : copy ${jetty.home}\modules\demo.d\demo-moved-context.xml to ${jetty.base}\webapps\demo-moved-context.xml
INFO  : copy ${jetty.home}\modules\demo.d\demo-jndi.xml to ${jetty.base}\webapps\demo-jndi.xml
INFO  : download https://repo1.maven.org/maven2/org/eclipse/jetty/demos/demo-jndi-webapp/11.0.0/demo-jndi-webapp-11.0.0.war to ${jetty.base}\webapps\demo-jndi.war
INFO  : mkdir ${jetty.base}\lib\ext
INFO  : download https://repo1.maven.org/maven2/jakarta/mail/jakarta.mail-api/2.0.0-RC4/jakarta.mail-api-2.0.0-RC4.jar to ${jetty.base}\lib\ext\jakarta.mail-api-2.0.0-RC4.jar
INFO  : download https://repo1.maven.org/maven2/jakarta/transaction/jakarta.transaction-api/2.0.0-RC1/jakarta.transaction-api-2.0.0-RC1.jar to ${jetty.base}\lib\ext\jakarta.transaction-api-2.0.0-RC1.jar
INFO  : copy ${jetty.home}\modules\demo.d\demo-rewrite-rules.xml to ${jetty.base}\etc\demo-rewrite-rules.xml
INFO  : mkdir ${jetty.base}\work
INFO  : download https://repo1.maven.org/maven2/org/eclipse/jetty/demos/demo-async-rest-webapp/11.0.0/demo-async-rest-webapp-11.0.0.war to ${jetty.base}\webapps\demo-async-rest.war
INFO  : copy ${jetty.home}\modules\rewrite\rewrite-rules.xml to ${jetty.base}\etc\rewrite-rules.xml
INFO  : download https://repo1.maven.org/maven2/org/eclipse/jetty/demos/demo-mock-resources/11.0.0/demo-mock-resources-11.0.0.jar to ${jetty.base}\lib\ext\demo-mock-resources-11.0.0.jar
INFO  : copy ${jetty.home}\modules\demo.d\demo-jaas.xml to ${jetty.base}\webapps\demo-jaas.xml
INFO  : copy ${jetty.home}\modules\demo.d\demo-login.conf to ${jetty.base}\etc\demo-login.conf
INFO  : copy ${jetty.home}\modules\demo.d\demo-login.properties to ${jetty.base}\etc\demo-login.properties
INFO  : download https://repo1.maven.org/maven2/org/eclipse/jetty/demos/demo-jaas-webapp/11.0.0/demo-jaas-webapp-11.0.0.war to ${jetty.base}\webapps\demo-jaas.war
INFO  : mkdir ${jetty.base}\webapps\root
INFO  : mkdir ${jetty.base}\webapps\root\images
INFO  : copy ${jetty.home}\modules\demo.d\root\index.html to ${jetty.base}\webapps\root\index.html
INFO  : copy ${jetty.home}\modules\demo.d\root\jetty.css to ${jetty.base}\webapps\root\jetty.css
INFO  : copy ${jetty.home}\modules\demo.d\root\images\jetty-header.jpg to ${jetty.base}\webapps\root\images\jetty-header.jpg
INFO  : copy ${jetty.home}\modules\demo.d\root\images\webtide_logo.jpg to ${jetty.base}\webapps\root\images\webtide_logo.jpg
INFO  : Base directory was modified

user@DESKTOP-96FRN6B MINGW64 /d/ccc109/egov/java/00-install/03-jetty (master)
$ java -jar $JETTY_HOME/start.jar
2021-01-02 15:12:57.310:WARN :oe.jetty:main: demo-realm is deployed. DO NOT USE IN PRODUCTION!
2021-01-02 15:12:57.718:INFO :oejs.Server:main: jetty-11.0.0; built: 2020-12-02T21:37:29.286Z; git: 432f896d7a4555fcc81f38108757ea0aca8788e6; jvm 15.0.1+9-18
2021-01-02 15:12:59.093:INFO :oejus.SslContextFactory:main: x509=X509@72bc6553(mykey,h=[localhost],w=[]) for Server@70cf32e3[provider=null,keyStore=file:///D:/ccc109/egov/java/00-install/03-jetty/etc/test-keystore.p12,trustStore=file:///D:/ccc109/egov/java/00-install/03-jetty/etc/test-keystore.p12]
2021-01-02 15:13:00.222:INFO :oejdp.ScanningAppProvider:main: Deployment monitor [file:///D:/ccc109/egov/java/00-install/03-jetty/webapps/]
2021-01-02 15:13:03.830:WARN :oejshC.test_jndi:main: The test-jndi webapp is deployed. DO NOT USE IN PRODUCTION!
2021-01-02 15:13:05.433:INFO :oejss.DefaultSessionIdManager:main: Session workerName=node0
2021-01-02 15:13:06.071:INFO :oejsh.ContextHandler:main: Started o.e.j.w.WebAppContext@b978d10{Test JNDI WebApp,/test-jndi,file:///D:/ccc109/egov/java/00-install/03-jetty/work/jetty-0_0_0_0-8443-demo-jndi_war-_test-jndi-any-/webapp/,AVAILABLE}{D:\ccc109\egov\java\00-install\03-jetty\webapps/demo-jndi.war}
2021-01-02 15:13:06.524:WARN :oejshC.test_jaas:main: The test-jaas webapp is deployed. DO NOT USE IN PRODUCTION!
2021-01-02 15:13:06.965:INFO :oejsh.ContextHandler:main: Started o.e.j.w.WebAppContext@20ca951f{JAAS Test,/test-jaas,file:///D:/ccc109/egov/java/00-install/03-jetty/work/jetty-0_0_0_0-8443-demo-jaas_war-_test-jaas-any-/webapp/,AVAILABLE}{D:\ccc109\egov\java\00-install\03-jetty\webapps/demo-jaas.war}
2021-01-02 15:13:07.102:INFO :oejsh.ContextHandler:main: Started o.e.j.s.h.MovedContextHandler@3688eb5b{/oldContextPath,null,AVAILABLE}
2021-01-02 15:13:09.071:WARN :oejshC.demo_proxy:main: The test-proxy webapp is deployed. DO NOT USE IN PRODUCTION!
2021-01-02 15:13:10.865:INFO :oejsh.ContextHandler:main: Started o.e.j.w.WebAppContext@69f1a286{Transparent Proxy WebApp,/proxy,file:///D:/ccc109/egov/java/00-install/03-jetty/work/jetty-0_0_0_0-8443-demo-proxy_war-_demo-proxy-any-/webapp/,AVAILABLE}{D:\ccc109\egov\java\00-install\03-jetty\webapps\demo-proxy.war}
2021-01-02 15:13:11.744:WARN :oejshC.test_spec:main: The test-spec webapp is deployed. DO NOT USE IN PRODUCTION!
2021-01-02 15:13:11.744:INFO :oe.jetty:main: WEB-INF/lib/jetty-util.jar logging used!
2021-01-02 15:13:12.691:INFO :oejsh.ContextHandler:main: Started o.e.j.w.WebAppContext@345f69f3{Test Annotations WebApp,/test-spec,[file:///D:/ccc109/egov/java/00-install/03-jetty/work/jetty-0_0_0_0-8443-demo-spec_war-_test-spec-any-/webapp/, jar:file:///D:/ccc109/egov/java/00-install/03-jetty/work/jetty-0_0_0_0-8443-demo-spec_war-_test-spec-any-/webapp/WEB-INF/lib/demo-web-fragment-11.0.0.jar!/META-INF/resources],AVAILABLE}{D:\ccc109\egov\java\00-install\03-jetty\webapps/demo-spec.war}
2021-01-02 15:13:13.917:WARN :oejshC.test:main: The test-jetty webapp is deployed. DO NOT USE IN PRODUCTION!
2021-01-02 15:13:15.350:INFO :oejsh.ContextHandler:main: Started o.e.j.w.WebAppContext@4f5991f6{Test WebApp,/test,file:///D:/ccc109/egov/java/00-install/03-jetty/work/jetty-0_0_0_0-8443-demo-jetty_war-_test-any-/webapp/,AVAILABLE}{D:\ccc109\egov\java\00-install\03-jetty\webapps/demo-jetty.war}
2021-01-02 15:13:16.907:WARN :oejshC.demo_async_rest:main: The async-rest webapp is deployed. DO NOT USE IN PRODUCTION!
2021-01-02 15:13:17.250:INFO :oejsh.ContextHandler:main: Started o.e.j.w.WebAppContext@484094a5{Async REST Webservice Example,/demo-async-rest,[file:///D:/ccc109/egov/java/00-install/03-jetty/work/jetty-0_0_0_0-8443-demo-async-rest_war-_demo-async-rest-any-/webapp/, jar:file:///D:/ccc109/egov/java/00-install/03-jetty/work/jetty-0_0_0_0-8443-demo-async-rest_war-_demo-async-rest-any-/webapp/WEB-INF/lib/demo-async-rest-jar-11.0.0.jar!/META-INF/resources],AVAILABLE}{D:\ccc109\egov\java\00-install\03-jetty\webapps\demo-async-rest.war}
2021-01-02 15:13:18.050:INFO :oejsh.ContextHandler:main: Started o.e.j.w.WebAppContext@38234a38{root,/,file:///D:/ccc109/egov/java/00-install/03-jetty/webapps/root/,AVAILABLE}{D:\ccc109\egov\java\00-install\03-jetty\webapps\root}
2021-01-02 15:13:18.390:INFO :oejs.AbstractConnector:main: Started ServerConnector@47aea7b0{SSL, (ssl, alpn, h2, http/1.1)}{0.0.0.0:8443}
2021-01-02 15:13:18.401:INFO :oejs.AbstractConnector:main: Started ServerConnector@aced190{HTTP/1.1, (http/1.1)}{0.0.0.0:8080}
2021-01-02 15:13:18.695:INFO :oejs.Server:main: Started Server@54bff557{STARTING}[11.0.0,sto=5000] @34985ms

```

然後看 http://localhost:8080/

```
user@DESKTOP-96FRN6B MINGW64 /d/ccc109/egov/java/00-install/03-jetty (master)
$ ls ./ -R | grep jsp
jsp
./work/jetty-0_0_0_0-8443-demo-async-rest_war-_demo-async-rest-any-/jsp:    
jsp
./work/jetty-0_0_0_0-8443-demo-jaas_war-_test-jaas-any-/jsp:
logout.jsp
jsp
./work/jetty-0_0_0_0-8443-demo-jetty_war-_test-any-/jsp:
jsp
./work/jetty-0_0_0_0-8443-demo-jetty_war-_test-any-/webapp/jsp:
bean1.jsp
bean2.jsp
dump.jsp
expr.jsp
jstl.jsp
tag.jsp
tag2.jsp
tagfile.jsp
./work/jetty-0_0_0_0-8443-demo-jetty_war-_test-any-/webapp/jsp/foo:
foo.jsp
jsp
./work/jetty-0_0_0_0-8443-demo-jndi_war-_test-jndi-any-/jsp:
jsp
./work/jetty-0_0_0_0-8443-demo-proxy_war-_demo-proxy-any-/jsp:
jsp
./work/jetty-0_0_0_0-8443-demo-spec_war-_test-spec-any-/jsp:
dynamic.jsp
logout.jsp
jsp
./work/jetty-0_0_0_0-8443-root-_-any-/jsp:
```

## 

```
user@DESKTOP-96FRN6B MINGW64 /d/ccc109/egov/java/00-install/03-jetty/work/jetty-0_0_0_0-8443-demo-jaas_war-_test-jaas-any-/webapp (master)
$ ls *.jsp
logout.jsp
```

File: logout.jsp

```
<%@ page contentType="text/html; charset=UTF-8" %>
<html>
<head>
    <title>Logout</title>
</head>

<body>
<%
    HttpSession s = request.getSession(false);
    s.invalidate();
   %>
   <h1>Logout</h1>

   <p>You are now logged out.</p> 
   <a href="auth.html"/>Login</a>
</body>

</html>

```