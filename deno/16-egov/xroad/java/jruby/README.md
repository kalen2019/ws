# jruby

## jframe1.rb

```
user@DESKTOP-96FRN6B MINGW64 /d/ccc109/egov/java/jruby (master)
$ jruby jframe1.rb
```

## jirb

```
user@DESKTOP-96FRN6B MINGW64 /d/ccc109/egov/java/jruby/calljava (master)
$ jirb
irb(main):001:0> puts "Hello world!"
Hello world!
=> nil
irb(main):002:0> exit

user@DESKTOP-96FRN6B MINGW64 /d/ccc109/egov/java/jruby/calljava (master)
$ jruby -e "puts 'Hello World'"
Hello World
```
