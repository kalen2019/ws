# CallingJavaFromJRuby

* https://github.com/jruby/jruby/wiki/CallingJavaFromJRuby

```sh
user@DESKTOP-96FRN6B MINGW64 /d/ccc109/egov/java/jruby/calljava (master)
$ ./run.sh
Hello from ruby
value: Bar
value: baz
value: foo
Hello from Default
NoMethodError: undefined method `hello' for nil:NilClass
  <main> at calljava.rb:17
```