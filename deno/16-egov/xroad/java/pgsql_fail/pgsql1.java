import java.sql.Connection;
import java.sql.DriverManager;

public class pgsql1 {
   public static void main(String args[]) {
      System.out.println("main: run()");
      Connection c = null;
      try {
         Class.forName("org.postgresql.Driver");
         c = DriverManager
            .getConnection("jdbc:postgresql://localhost:5432/mydb",
            "postgres", "123");
      } catch (Exception e) {
         System.out.println("Error: ccc");
         e.printStackTrace();
         System.err.println(e.getClass().getName()+": "+e.getMessage());
         System.exit(0);
      }
      System.out.println("Opened database successfully");
   }
}
