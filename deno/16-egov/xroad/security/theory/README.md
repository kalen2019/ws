# 


## C. E. SHANNON

* [A Symbolic Analysis of Relay and Switching Circuits](https://www.cs.virginia.edu/~evans/greatworks/shannon38.pdf)
* [A Mathematical Theory of Communication](http://people.math.harvard.edu/~ctm/home/text/others/shannon/entropy/entropy.pdf)
* [獨自搞定電腦與通訊的理論基礎，卻罕為人知的天才——夏農│《電腦簡史》數位時代（四）](https://pansci.asia/archives/191130)
