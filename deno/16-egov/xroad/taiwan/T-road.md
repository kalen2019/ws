# T-road

* [【全球數位轉型浪潮政府如何跟上？】貼合社會文化需求，因地制宜打造台灣專有的 T-Road](https://buzzorange.com/techorange/2020/08/25/troad-in-taiwan/)
* [智慧政府 T-Road 計畫台灣推動數位治理關鍵基礎建設](https://home.kpmg/tw/zh/home/insights/2020/08/tw-digital-governance-t-road.html)
* [加速政府資料傳輸與安全應用－T-Road 架構與安全機制 楊蘭堯 國家發展委員會資訊管理處高級分析師](https://ws.ndc.gov.tw/Download.ashx?u=LzAwMS9hZG1pbmlzdHJhdG9yLzEwL3JlbGZpbGUvMC8xNDAwOS85ZTY5NmE4NS1kOGNjLTRiNjktOTA0ZC0xYWU0ZGUzNWI5MzgucGRm&n=5pS%2F562W5paw55%2BlMDUt5Yqg6YCf5pS%2F5bqc6LOH5paZ5YKz6Ly46IiH5a6J5YWo5oeJ55So4pSA4pSAVC1Sb2Fk5p625qeL6IiH5a6J5YWo5qmf5Yi2LnBkZg%3D%3D&icon=..pdf&fbclid=IwAR3IdvOhdnDj5zicLirZPcGymEoZV4z8GvdTYSmF7NRDiXqWVPOM99RkVnc)
* [T-Road入口網規劃說明 國家發展委員會](https://ws.webguide.nat.gov.tw/Download.ashx?u=LzAwMS9VcGxvYWQvMS9yZWxmaWxlLzg0NTMvMjk1NS9jYzVlMmMzYy0zM2Q3LTRkOGUtYTdjYy02NTIwY2ZjODhiMWYucGRm&n=6K2w6aGMMy1ULVJvYWTlhaXlj6PntrLopo%2FlioPoqqrmmI4ucGRm)