# X-Road: Use Case


[UC-SS] Security Server Management
[UC-CP] Configuration Proxy
[UC-CS] Central Server Management
[UC-FED] Federation
[UC-MEMBER] Member Management
[UC-GCONF] Global Configuration Distribution
[UC-TRUST] Trust Service Management
[UC-MESS] Member Communication
[UC-SERVICE] Service Management
[UC-OPMON] Use Case Model for Operational Monitoring Daemon