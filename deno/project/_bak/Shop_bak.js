const Shop = {}

Shop.start = function (id) {
  var shop = Model.loadShop(id)
  if (shop == null) {
    alert('您尚未選擇商店，請先《逛商場》選擇商店！')
    return
  }
  Router.go('#setting')
  /*
  Ui.show(`
  <h2>${shop.name} (${shop.id})</h2>
  <ol>
    <li><a href="#pos">新增訂單</a></li>
    <li><a href="#report">本日報表</a></li>
    <li><a href="#setting">商店設定</a></li>
  </ol>
  `)
  */
}
