const Db = {}

Db.normal = function (key) {
  return key.toLowerCase() // 轉小寫，正規化！
}

Db.save = function (key, value) {
  var k = Db.normal(key)
  Cache.save(k, value)
  localStorage.setItem(k, JSON.stringify(value))
}

Db.load = function (key) {
  var k = Db.normal(key)
  var value = Cache.load(k, value)
  if (value != null) return value
  var json = localStorage.getItem(k)
  if (json != null) {
    value = JSON.parse(json)
    Cache.save(k, value)
    return value
  }
  return null
}

Db.add = function (key, n) {
  var k = Db.normal(key)
  var value = Db.load(k)
  if (value == null) return null
  value += n
  Db.save(k, value)
  return value
}
