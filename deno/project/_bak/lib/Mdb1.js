const db = new Nedb()

Mdb = {}

function promisify(f) {
  return function() {
    var args = [...arguments]
    return new Promise(function (resolve, reject) {
      // 下面這行會強制塞入 callback function, 導致 db 不傳回 cursor 而傳回 exec 解完後的值。
      args.push(function (err, result) {
        if (err) reject(err); else resolve(result);
      })
      f(...args)
    })
  }
}

Mdb.insert = promisify(db.insert.bind(db))
Mdb.findOne = promisify(db.findOne.bind(db))
Mdb.count = promisify(db.count.bind(db))
Mdb.update = promisify(db.update.bind(db))
Mdb.remove = promisify(db.remove.bind(db))
Mdb.ensureIndex = promisify(db.ensureIndex.bind(db))
Mdb.removeIndex = promisify(db.removeIndex.bind(db))

Mdb.find = function (q, options={}) {
  var {sort, skip, limit, projection} = options
  var cursor = db.find(q)
  if (sort) cursor = cursor.sort(sort)
  if (skip) cursor = cursor.skip(skip)
  if (limit) cursor = cursor.limit(limit)
  if (projection) cursor = cursor.projection(projection)
  return new Promise(function (resolve, reject) {
    cursor.exec(function (err, result) {
      if (err) reject(err); else resolve(result);
    })
  })
}
