const Tb = {}

Tb.cell2text = function (v, type) {
  if (v == null) return ''
  switch (type) {
    case 'boolean': return (v == false) ? '0' : '1'
    case 'number': return ''+v
  }
  return v
}

Tb.text2cell = function (text, type) {
  if (text.trim() === '') return undefined
  switch (type) {
    case 'boolean': return (text == true || text == "true")
    case 'number': return parseFloat(text)
  }
  return text
}

Tb.attachTo = function (selector, args) {
  var node = document.querySelector(selector)
  var html = `
  <p><i class="add fa fa-plus-circle" aria-hidden="true"></i> <i class="save fa fa-save"></i></p>
  ${Tb.table2html(args)}
  `
  node.innerHTML = html
  node.querySelector('.save').addEventListener("click", function () { Tb.updateRecords(args); })
  node.querySelector('.add').addEventListener("click", function () { Tb.addRow(args); })
}

Tb.table2html = function (args) {
  var {fields, types, titles, records, styles, editable, id} = args
  titles = titles || fields
  var fcells = []
  for (var fi=0; fi<fields.length; fi++) {
    fcells[fi] = `<th>${titles[fi]}</th>`
  }
  var header = `<tr>${fcells.join('\n')}</tr>`
  var rows = []
  for (var record of records) {
    var cells = []
    for (var fi=0; fi<fields.length; fi++) {
      var style = (styles == null) ? '' : styles[fi]
      var cell = Tb.cell2text(record[fields[fi]], types[fi])
      cells.push(`<td contenteditable="${editable}" style="${style}">${cell}</td>`)
    }
    rows.push(`<tr>${cells.join('')}</tr>`)
  }
  var body = rows.join('\n')
  return `
<table id="${id}">
  <thead>${header}</thead>
  <tbody>${body}</tbody>
</table>`
}

Tb.updateRecords = function (args) {
  var { records, fields, types, id } = args
  var rows = document.querySelectorAll(`#${id} tr`)
  for (var ri=1; ri<rows.length; ri++) {
    var cells = rows[ri].querySelectorAll('td')
    var record = records[ri-1] || {}
    for (var ci=0; ci<cells.length; ci++) {
      var cell = cells[ci].innerText.trim()
      record[fields[ci]] = Tb.text2cell(cell, types[ci])
    }
    records[ri-1] = record
  }
  args.records = records.slice(0, rows.length-1);
  var map = Tb.toMap(args.records, args.key)
  console.log(map)
  var list = Tb.toList(map)
  console.log(list)
}

Tb.addRow = function (args) {
  console.log("Tb.tableAddRow()")
  var { fields, id, editable } = args
  var tbody = document.querySelector(`#${id} tbody`)
  var row = []
  for (var fi=0; fi<fields.length; fi++) {
    row.push(`<td contenteditable="${editable}">&nbsp;</td>`)
  }
  tbody.innerHTML += `<tr>${row.join('')}</tr>`
}

Tb.toMap = function (list, key) {
  var map = {}
  for (var o of list) {
    map[o[key]] = o
  }
  return map
}

Tb.toList = function (map) {
  return Object.values(map)
}
