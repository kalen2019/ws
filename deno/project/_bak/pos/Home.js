const Home = {}

Home.html = `
<h2>文學式 POS 系統</h2>

<p>這是一個《很像電子書》的購物商店系統，您可以在這裡使用下列服務：</p>

<table>
<tr><th>服務</th><th>流程</th></tr>
<tr>
  <td>基本</td>
  <td>
    <a href="#signup">註冊</a> / 
    <a href="#login">登入</a> / 
    <a href="#logout">登出</a> / 
    <a href="#my">我的專區</a>
  </td>
</tr>
<tr>
  <td>開店</td>
  <td>
    <a href="#my">我的專區</a> / 
    <a href="#setting">商店設定</a> / 
    <a href="#pos">訂單系統</a> / 
    <a href="#report">報表結算</a>
  </td>
</tr>
<tr>
  <td>購物</td>
  <td>
    <a href="#market">逛商場</a> / 
    <a href="#pos">購物</a> / 
    <a href="#report">結帳</a>
  </td>
</tr>
</table>
`

Home.start = function () {
  Model.init()
  Ui.show(Home.html)
}

window.addEventListener('load', ()=> {
  Router
    .route(/#home/, Home.start)
    .route(/#signup/, Signup.start)
    .route(/#login/, Login.start)
    .route(/#logout/, Logout.start)
    .route(/#market/, Market.start)
    .route(/#pos/, Pos.start)
    .route(/#report/, Report.start)
    .route(/#setting/, Setting.start)
    .route(/#shop/, Shop.start)
    .route(/#my/, My.start)
    .default(()=>Router.go('#home'))
  window.addEventListener('hashchange', Router.onhash)
  Router.onhash()
})
