const Login = {}

Login.html = `
<h1>Login</h1>
<form action="/login" method="post">
  <p><input type="text" placeholder="username" name="username"></p>
  <p><input type="password" placeholder="password" name="password"></p>
  <p><input type="submit" value="Login"></p>
  <p>New user? <a href="/signup">Create an account</p>
</form>
`

Login.start = function () {
  Ui.show(Login.html)
}
