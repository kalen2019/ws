const Logout = {}

Logout.html = `
<h1>Logout</h1>
<form action="/logout" method="post">
  <p>請按下列按鈕登出！</p>
  <p><input type="submit" value="Logout"></p>
</form>
`

Logout.start = function () {
  Ui.show(Logout.html)
}