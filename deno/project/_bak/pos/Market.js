const Market = {}

Market.html = `
<h2>商店市場</h2>
<form action="/search" method="post">
  <p><input type="text" placeholder="query" name="query"><input type="submit" value="搜尋"></p>
</form>
<h2>商店列表</h2>
<ol>
  <li><a href="#shop/teashop">茶舖子</a></li>
  <li><a href="#shop/animalhost">動物寄養</a></li>
  <li><a href="#shop/dailycare">日間看護</a></li>
</ol>
`

Market.start = function () {
  Ui.show(Market.html)
}