const Model = {
  _shop:  {
    id: 'teashop',
    name: '茶舖子',
    address: '金門縣金寧鄉安美村湖南 33 號',
    tel: '082-333333',
    products: {
      '紅茶':{id:'紅茶', price:20},
      '綠茶':{id:'綠茶', price:20}, 
      '珍珠奶茶':{id:'珍珠奶茶', price:35},
    },
    addons: {
      '去冰':{id:'去冰', price:0},
      '半糖':{id:'半糖', price:0}, 
      '熱':{id:'熱', price:0},
      '加鮮奶':{id:'加鮮奶', price:10},
    },
    _animalhost:  {
      id: 'animalhost',
      name: '動物寄養',
      address: '金門縣金寧鄉安美村湖南 77 號',
      tel: '082-777777',
      products: {
        '白天貓狗寄養':{id:'白天貓狗寄養', price:300},
      },
      addons: {
        '不含夜間':{id:'不含夜間', price:0},
        '含夜間':{id:'含夜間', price:200},
      },
    },
    _childcare:  {
      id: 'patientcare',
      name: '病人照護',
      address: '金門縣金寧鄉安美村湖南 88 號',
      tel: '082-8888888',
      products: {
        '自行送來(白天)':{id:'自行送來(白天)', price:1200},
        '到府照料(白天)':{id:'到府照料(白天)', price:2000},
        '自行送來(日夜)':{id:'自行送來(日夜)', price:2000},
        '到府照料(日夜)':{id:'到府照料(日夜)', price:3000},
      },
      addons: {
        '自帶棉被':{id:'自帶棉被', price:-200},
        '不自帶棉被':{id:'不自帶棉被', price:0},
      },
    },
  },
  _user:  {
    id: 'ccc',
    name: '陳鍾誠',
    email: 'ccckmit@gmail.com',
    tel: '082-333333',
    shops: [ 
      {id:'teashop', name:'茶舖子' }
    ],
  }
}

Model.init = function () {
  if (Db.load('shop') == null) Db.save('shop', Model._shop)
  if (Db.load('user') == null) Db.save('user', Model._user)
  if (Db.load('order.count') == null) Db.save('order.count', 0)
}

Model.saveOrder = function (order) {
  var orderCount = Db.add('order.count', 1)
  order.time = Date.now()
  Db.save('order.'+orderCount, order)
}
