const My = {}

My.html = `
<h2>我的資料</h2>
<table>
  <thead><tr><th>欄位</th><th>內容</th></tr></thead>
  <tbody>
    <tr><td>帳號</td><td><input id="userId" type="text" value=""/></td></tr>
    <tr><td>密碼</td><td><input id="userPassword" type="password" value=""/></td></tr>
    <tr><td>姓名</td><td><input id="userName" type="text" value=""/></td></tr>
    <tr><td>信箱</td><td><input id="userEmail" type="text" value=""/></td></tr>
    <tr><td>電話</td><td><input id="userTel" type="text" value=""/></td></tr>
  </tbody>
</table>

<h2>我的商店</h2>
<div id="myshops"></div>
`

My.start = function () {
  Ui.show(My.html)
  var user = Db.load('user')
  Tb.attachTo('#myshops', {
    id: 'shopTable',
    fields: ["id", "name"],
    titles: ["代號", "店名"],
    types: ["string", "string"],
    records: user.shops,
    editable:true,
  })
}
