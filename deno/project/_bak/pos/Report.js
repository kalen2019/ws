const Report = {}

Report.html = `
  <div id="report" class="panel">
    <table>
      <thead><tr><th>代號</th><th>金額</th><th>日期</th><th>時間</th></tr></thead>
      <tbody id="reportBody"></tbody>
    </table>
    <div class="center">
      <label>本日總收入：</label>
      <label id="dayTotal"></label>
    </div>
    <p><button onclick="Report.checkout()">確認結帳</button></p>
    <p><a href="#shop"> <i class="fa fa-arrow-left" aria-hidden="true"></i> 回商店</a></p>
  </div>
  <div id="detail" class="panel" style="display:none">
    <table>
      <thead><tr><th>商品</th><th>附加</th><th>單價</th><th>數量</th></tr></thead>
      <tbody id="detailBody"></tbody>
    </table>
    <div>
      <p>總價：<label id="totalPrice"></label></p>
      <p><a onclick="Ui.showPanel('report')"> <i class="fa fa-arrow-left" aria-hidden="true"></i> 回到報表</a></p>
    </div>
  </div>
`

Report.start = function () {
  Ui.show(Report.html)
  Report.showReport()
}

Report.showReport = function () {
  Ui.id('reportBody').innerHTML = Report.orderListHtml()
  Ui.id('dayTotal').innerHTML = Report.dayTotal + ''
}

Report.showDetail = function (i) {
  Ui.showPanel('detail')
  let order = Db.load('order.'+i)
  Ui.id('detailBody').innerHTML = Report.orderDetailHtml(order)
  Ui.id('totalPrice').innerHTML = order.totalPrice
}

Report.orderListHtml = function () {
  list = []
  let dayTotal = 0
  var orderCount = Db.load('order.count')
  for (let i=1; i <= orderCount; i++) {
    let order = Db.load('order.'+i)
    dayTotal += order.totalPrice
    list.push(Report.orderRowHtml(i, order))
  }
  Report.dayTotal = dayTotal
  return list.join('\n')
}

Report.orderRowHtml = function (i, order) {
  let time = new Date(order.time)
  return '<tr><td><a onclick="Report.showDetail('+i+')">0' + i + '</a></td><td class="number">' + order.totalPrice + '</td><td>' + Lib.dateToString(time) + '</td><td>' + Lib.timeToString(time) + '</td></tr>'
}

Report.orderDetailHtml = function (order) {
  let detail = []
  let items = order.items
  for (let i=0; i<items.length; i++) {
    let item = items[i]
    detail.push('<tr><td>' + item.productId + '</td><td>' + item.addonId + '</td><td>' + item.price + '</td><td>' + item.quantity + '</td></tr>')
  }
  return detail.join('\n')
}

