const Setting = {}

Setting.html = `
<h2>商店資料</h2>
<table>
  <thead><tr><th>欄位</th><th>內容</th></tr></thead>
  <tbody>
    <tr><td>商店代號</td><td><input id="shopId" type="text" value=""/></td></tr>
    <tr><td>商店名稱</td><td><input id="shopName" type="text" value=""/></td></tr>
    <tr><td>地址</td><td><input id="shopAddress" type="text" value=""/></td></tr>
    <tr><td>電話</td><td><input id="shopTel" type="text" value=""/></td></tr>
  </tbody>
</table>
<h2>商品</h2>
<div id="products">
</div>
<h2>附加</h2>
<div id="addons">
</div>
<p><button onclick="Setting.save()">儲存設定</button></p>
<p><a href="#shop"> <i class="fa fa-arrow-left" aria-hidden="true"></i> 回商店</a></p>
`

Setting.start = function () {
  var shop = Db.load('shop')
  shop.productList = Tb.toList(shop.products)
  shop.addonList = Tb.toList(shop.addons)
  Ui.show(Setting.html)
  Ui.id('shopId').value = shop.id
  Ui.id('shopName').value = shop.name
  Ui.id('shopAddress').value = shop.address
  Ui.id('shopTel').value = shop.tel
  Tb.attachTo('#products', {
    id: 'productTable',
    fields: ["id", "price", "deleted"],
    titles: ["商品", "單價", "刪除"],
    types: ["string", "number", "boolean"],
    records: shop.productList,
    editable:true,
  })
  Tb.attachTo('#addons', {
    id: 'addonTable',
    fields: ["id", "price", "deleted"],
    titles: ["附加", "單價", "刪除"],
    types: ["string", "number", "boolean"],
    records: shop.addonList,
    editable:true,
  })
  Setting._shop = shop
}

Setting.save = function () {
  var tShop = {}
  try {
    tShop.name = Ui.id('shopName').value
    tShop.address = Ui.id('shopAddress').value
    tShop.tel = Ui.id('shopTel').value
    tShop.products = JSON.parse(Ui.id('products').value)
    tShop.addons = JSON.parse(Ui.id('addons').value)
  } catch (error) {
    alert('儲存失敗，請檢查格式是否有錯！\n', error)
    return
  }
  Db.save('shop', Setting._shop)
  alert('儲存成功！')
}