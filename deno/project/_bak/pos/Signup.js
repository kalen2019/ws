const Signup = {}

Signup.html = `
<h1>Signup</h1>
<form action="/signup" method="post">
  <p><input type="text" placeholder="username" name="username"></p>
  <p><input type="password" placeholder="password" name="password"></p>
  <p><input type="text" placeholder="email" name="email"></p>
  <p><input type="submit" value="Signup"></p>
</form>
`

Signup.start = function () {
  Ui.show(Signup.html)
}
