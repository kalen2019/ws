window.addEventListener('hashchange', function () {
  var tokens = window.location.hash.split('/')
  console.log('tokens=', tokens)
  switch (tokens[0]) {
    case '#pos': Pos.start(); break;
    case '#report': Report.start(); break;
    case '#setting': Setting.start(); break;
    case '#shop': Shop.start(); break;
    case '#user': User.start(); break;
    default: Shop.start(); break;
  }
})
