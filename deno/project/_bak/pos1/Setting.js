const Setting = {}

Setting.html = `
<h2>商店資料</h2>
<table>
  <thead><tr><th>欄位</th><th>內容</th></tr></thead>
  <tbody>
    <tr><td>商店名稱</td><td><input id="shopName" type="text" value=""/></td></tr>
    <tr><td>地址</td><td><input id="shopAddress" type="text" value=""/></td></tr>
    <tr><td>電話</td><td><input id="shopTel" type="text" value=""/></td></tr>
    <!--<tr><td>產品清單</td><td><textarea id="products"></textarea></td></tr>
    <tr><td>附加選項</td><td><textarea id="addons"></textarea></td></tr>-->
  </tbody>
</table>
<h2>商品</h2>
<div id="products">
</div>
<h2>附加</h2>
<div id="addons">
</div>
<br/>
<button onclick="Setting.save()">儲存設定</button>
<!--<button onclick="Shop.start()">回主選單</button>-->
`

Setting.start = function () {
  var tShop = Shop.profile
  Ui.show(Setting.html)
  Ui.id('shopName').value = tShop.name
  Ui.id('shopAddress').value = tShop.address
  Ui.id('shopTel').value = tShop.tel
  Tb.attachTo('#products', {
    id: 'productTable',
    fields: ["id", "price", "deleted"],
    titles: ["商品", "單價", "刪除"],
    types: ["string", "number", "boolean"],
    records: Shop.profile.productList,
    editable:true,
  })
  Tb.attachTo('#addons', {
    id: 'addonTable',
    fields: ["id", "price", "deleted"],
    titles: ["附加", "單價", "刪除"],
    types: ["string", "number", "boolean"],
    records: Shop.profile.addonList,
    editable:true,
  })
  // Ui.id('products').value = JSON.stringify(tShop.products, null, 2)
  // Ui.id('addons').value = JSON.stringify(tShop.addons, null, 2)
}

Setting.save = function () {
  var tShop = {}
  try {
    tShop.name = Ui.id('shopName').value
    tShop.address = Ui.id('shopAddress').value
    tShop.tel = Ui.id('shopTel').value
    tShop.products = JSON.parse(Ui.id('products').value)
    tShop.addons = JSON.parse(Ui.id('addons').value)
  } catch (error) {
    alert('儲存失敗，請檢查格式是否有錯！\n', error)
    return
  }
  Shop.profile = tShop
  localStorage.setItem('Shop', JSON.stringify(tShop))
  Ui.id('menuShopName').innerHTML = tShop.name
  alert('儲存成功！')
}