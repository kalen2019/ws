const Shop = {
  profile: {
    name: '茶舖子',
    address: '金門縣金寧鄉安美村湖南 33 號',
    tel: '082-333333',
    productList: [
      {id:'紅茶', price:20},
      {id:'綠茶', price:20}, 
      {id:'珍珠奶茶', price:35},
    ],
    addonList: [
      {id:'去冰', price:0},
      {id:'半糖', price:0}, 
      {id:'熱', price:0},
      {id:'加鮮奶', price:10},
    ],
  },
  orders: {
    count: 0,
  }
}

Shop.html = `
<h2>商店首頁</h2>
<ol>
  <li><a href="#pos">新增訂單</a></li>
  <li><a href="#report">本日報表</a></li>
  <li><a href="#setting">商店設定</a></li>
</ol>
`

Shop.loadProfile = function () {
  var shopJson = localStorage.getItem('Shop')
  if (shopJson) {
    Shop.profile = JSON.parse(shopJson)
  } else {
    var p = Shop.profile
    p.products = Tb.toMap(p.productList, "id")
    p.addons = Tb.toMap(p.addonList, "id")
  }
}

Shop.start = function () {
  Shop.init()
  // Ui.id('menuShopName').innerHTML = Shop.profile.name
  Shop.loadProfile()
  Ui.show(Shop.html)
}

Shop.init = function () {
  Shop.orders.count = localStorage.getItem('Pos.Order.count')
  if (Shop.orders.count == null) {
    Shop.orders.count = 0
    localStorage.setItem('Pos.Order.count', Shop.orders.count)
  }
}

Shop.saveOrder = function (Order) {
  localStorage.setItem('Pos.Order.count', ++ Shop.orders.count)
  Order.time = Date.now()
  Order.submitted = true
  localStorage.setItem('Pos.Order.' + Shop.orders.count, JSON.stringify(Order))
}

Shop.getOrder = function (i) {
  let orderJson = localStorage.getItem('Pos.Order.'+i)
  if (orderJson == null) return null
  return JSON.parse(orderJson)
}
