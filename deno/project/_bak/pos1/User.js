const User = {}

User.html = `
<h2>用戶專區</h2>
<ol>
  <li><a href="#myorder">我的訂單</a></li>
  <li><a href="#mydata">我的基本資料</a></li>
  <li><a href="#myshop">我的商店</a></li>
  <li><a href="#mylove">我的最愛</a></li>
</ol>
`

User.start = function () {
  Ui.show(User.html)
}
