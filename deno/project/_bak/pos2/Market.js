const Market = {}

Market.html = `
<h2>商店市場</h2>
<ol>
  <li><a href="#shop/teashop">茶舖子</a></li>
  <li><a href="#shop/meatshop">肉舖子</a></li>
</ol>
`

Market.start = function () {
  Ui.show(Market.html)
}