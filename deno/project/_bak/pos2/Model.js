const Model = {
  _shop:  {
    name: '茶舖子',
    address: '金門縣金寧鄉安美村湖南 33 號',
    tel: '082-333333',
    products: {
      '紅茶':{id:'紅茶', price:20},
      '綠茶':{id:'綠茶', price:20}, 
      '珍珠奶茶':{id:'珍珠奶茶', price:35},
    },
    addons: {
      '去冰':{id:'去冰', price:0},
      '半糖':{id:'半糖', price:0}, 
      '熱':{id:'熱', price:0},
      '加鮮奶':{id:'加鮮奶', price:10},
    },
  }
}

Model.init = function () {
  if (Db.load('shop') == null) {
    Db.save('shop', Model._shop)
  }
  if (Db.load('order.count') == null) {
    Db.save('order.count', 0)
  }
}

Model.saveOrder = function (order) {
  var orderCount = Db.add('order.count', 1)
  order.time = Date.now()
  Db.save('order.'+orderCount, order)
}
