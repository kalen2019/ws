const Shop = {
  profile: {
    name: '茶舖子',
    address: '金門縣金寧鄉安美村湖南 33 號',
    tel: '082-333333',
    productList: [
      {id:'紅茶', price:20},
      {id:'綠茶', price:20}, 
      {id:'珍珠奶茶', price:35},
    ],
    addonList: [
      {id:'去冰', price:0},
      {id:'半糖', price:0}, 
      {id:'熱', price:0},
      {id:'加鮮奶', price:10},
    ],
/*    products: {
      '紅茶': {price:20},
      '綠茶': {price:20}, 
      '珍珠奶茶': {price:35},
    },
    addons: {
      '去冰': {price:0}, 
      '半糖': {price:0}, 
      '熱': {price:0}, 
      '加鮮奶': {price:10},
    },*/
  },
  orders: {
    count: 0,
  }
}

Shop.html = `
<div>
  <button class="big" onclick="Pos.start()">新增訂單</button><br/><br/>
  <button class="big" onclick="Report.start()">本日報表</button><br/><br/>
  <button class="big" onclick="Setting.start()">商店設定</button><br/><br/>
</div>
`

Shop.loadProfile = function () {
  var shopJson = localStorage.getItem('Shop')
  if (shopJson) {
    Shop.profile = JSON.parse(shopJson)
  } else {
    var p = Shop.profile
    p.products = Tb.toMap(p.productList, "id")
    p.addons = Tb.toMap(p.addonList, "id")
    // delete p.productList
    // delete p.addonList
  }
}

Shop.start = function () {
  Shop.init()
  Ui.id('menuShopName').innerHTML = Shop.profile.name
  Shop.loadProfile()
  Ui.show(Shop.html)
}

Shop.init = function () {
  Shop.orders.count = localStorage.getItem('Pos.Order.count')
  if (Shop.orders.count == null) {
    Shop.orders.count = 0
    localStorage.setItem('Pos.Order.count', Shop.orders.count)
  }
}

Shop.saveOrder = function (Order) {
  localStorage.setItem('Pos.Order.count', ++ Shop.orders.count)
  Order.time = Date.now()
  Order.submitted = true
  localStorage.setItem('Pos.Order.' + Shop.orders.count, JSON.stringify(Order))
}

Shop.getOrder = function (i) {
  let orderJson = localStorage.getItem('Pos.Order.'+i)
  if (orderJson == null) return null
  return JSON.parse(orderJson)
}
