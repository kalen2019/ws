/*
商店名稱	茶舖子
地址	金門縣金寧鄉安美村湖南 33 號
電話	082-333333
產品清單	
{
  "紅茶": 20,
  "綠茶": 20,
  "珍珠奶茶": 35
}

客戶 ccc (密碼 123, email:ccc@nqu.edu.tw)

訂單 1 : ccc 訂購 紅茶 3 杯，綠茶 2 杯
*/

import { db } from "../db.js"

db.query("DROP TABLE IF EXISTS shops")
db.query("DROP TABLE IF EXISTS products")
db.query("DROP TABLE IF EXISTS users")
db.query("DROP TABLE IF EXISTS orders")
db.query("DROP TABLE IF EXISTS order_items")
db.query("CREATE TABLE shops (sid TEXT, name TEXT, address TEXT, tel TEXT)")
db.query("CREATE TABLE users (uid TEXT, name TEXT, password TEXT, email TEXT)")
db.query("CREATE TABLE products (pid INTEGER PRIMARY KEY AUTOINCREMENT, sid TEXT, name TEXT, price REAL)")
db.query("CREATE TABLE orders (oid INTEGER PRIMARY KEY AUTOINCREMENT, uid TEXT, sid TEXT, price REAL)")
db.query("CREATE TABLE order_items (oid INTEGER, pid INTEGER, count REAL)")

db.query("INSERT INTO shops (sid, name, address, tel) VALUES (?, ?, ?, ?)", ['teashop', '茶舖子', '金門縣金寧鄉安美村湖南 33 號', '082-333333'])

db.query("INSERT INTO products (sid, name, price) VALUES (?, ?, ?)",  ['teashop', '紅茶', 20])
db.query("INSERT INTO products (sid, name, price) VALUES (?, ?, ?)", ['teashop', '綠茶', 20])
db.query("INSERT INTO products (sid, name, price) VALUES (?, ?, ?)", ['teashop', '珍珠奶茶', 35])

db.query("INSERT INTO users (uid, name, password, email) VALUES (?, ?, ?, ?)", ['ccc', '陳鍾誠', '123', 'ccc@nqu.edu.tw'])

db.query("INSERT INTO orders (uid, sid, price) VALUES (?, ?, ?)", ['ccc', 'teashop', 100]) // 訂單 1: ccc 訂購 茶舖子 100 元

db.query("INSERT INTO order_items (oid, pid, count) VALUES (?, ?, ?)", [1, 1, 3]) // 訂單 1: 紅茶 3 杯
db.query("INSERT INTO order_items (oid, pid, count) VALUES (?, ?, ?)", [1, 2, 2]) // 訂單 1: 綠茶 2 杯

db.dumpTable('shops')
db.dumpTable('products')
db.dumpTable('users')
db.dumpTable('orders')
db.dumpTable('order_items')

let r = db.query("SELECT oid, uid, sid, price FROM orders WHERE sid=?", ['teashop'])
for (const o of r) {
  console.log(o)
}

db.close()

