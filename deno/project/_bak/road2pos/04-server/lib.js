export async function ctxParse(ctx) {
  const body = ctx.request.body() // content type automatically detected
  console.log('body = ', body)
  if (body.type === "json") {
    let data = await body.value
    return data
  }
  return null
}
